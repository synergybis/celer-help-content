# Celerform Help Content

## Versions

### Add a version

- Duplicate the most recent version directory and change the name of the folder to the new version name. This new folder's name is the name of the version and what will be used throughout the application.

- Follow the instructions for Sections and Pages to modify the content of the version.

### Delete a version

- Delete the corresponding folder in the `versions` directory. (i.e: to remove 0.0.1, delete the `versions/0.0.1` directory).

## Sections

### Add a section

- Open `config.json` in the version route.
- Add a new object in the following format:

```
"sectionName": {
    title: "Section",
    icon: "build",
    pages: {
      home: "Creating a Form",
    },
  },
```

- `"sectionName"` is the name of the route. No spaces, dashes, or underscores.
- `title` is the name of the section.
- `pages` is an object that contains all pages. Every section **must** have a page with the route `home`. (Refer to Add a Page on how to do that).
- Create a new directory in `<version>/<section>`. `<section>` refers to the section route.

### Delete a section

Delete the respective object from `config.json`, then delete the respective directory.
**NOTE:** Do not delete the form section, it is required.

### Update a section

Find the section object you want to edit and change the respective properties.
**NOTE:** Do not change the widgets & types page properties of the form section.

## Pages

### Add a page

- Open `config.json` in the version root.
- Under the `pages` object of the section you want to add a page to, add a new object in the following format:

```
pages: {
   "route": "Title of the Page"
}
```

- `"route"` refers to the route of the page. This must be unique and cannot contain spaces.
- `"Title of the Page"` refers to the actual heading of the page. This can contain any characters.

- Under the directory `<version>/<section>/` add a new markdown with the same name as the route. (i.e: `route.md`).
- Add contents to the .md file. **Do not leave the MD file empty.**

### Delete a page

Remove the respective object from the `pages` parent and delete the corresponding .MD file.

### Update a page

- Open the .MD file and edit it.