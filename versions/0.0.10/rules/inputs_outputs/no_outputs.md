This component does not produce outputs.

Editor note: currently there shouldn't be any rule components with no output, so if this shows up it's an error.