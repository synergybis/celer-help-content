![](../../../images/Then-String-Add.png)

Combines two strings of text together. Can be set text, text from widgets, or text from a previous rule result.
