![](../../../images/Then-Number-Compute.png)

Compute rules compute the results of algebraic equations created by users. Users implement the equation using variables when creating the rule, using static numbers or user inputs as variables. Calculate rules can combine any number of mathematical operations into the same equation. Calculate rules take order of operations and parenthesis into account, and can use up to 26 variables (one for each letter of the alphabet).

Compute functions include:

Addition +

Subtraction -

Multiplication *

Division /

Exponential ^

Difference - diff()

Factorial !

Natural logarithm - ln()

Rounding - round()

Sine - sin()

Cosine - cos()

Tangent - tan()

Ceiling - ceil()

Floor - floor()

Absolute value - abs()

e<sup>x</sup> - exp()