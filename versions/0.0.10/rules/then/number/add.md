![](../../../images/Then-Number-Add.png)

Adds together set numbers, numbers from widgets, or numerical results from previous rules.
