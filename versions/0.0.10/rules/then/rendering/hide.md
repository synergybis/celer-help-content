![](../../../images/Then-Rendering-Hide.png)

Makes a visible widget hidden from the form.
