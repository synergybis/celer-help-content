![](../../../images/When-Generic-Equals.png)

Activates the rule when a widget or rule’s final value is equal to a set value, or another widget.
