![](../../../images/When-Generic-Is-Empty.png)

Activates the rule when a widget doesn’t contain a value.
