![](../../../images/When-Number-Less-Than.png)

Activates the rule when the widget contains a number with a lower value than a set number, or a number in another widget.
