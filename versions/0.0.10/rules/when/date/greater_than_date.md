![](../../../images/When-Date-Greater-Than.png)

Activates the rule when a widget contains a date later than a set date, or later than a date in another widget.
