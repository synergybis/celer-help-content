![](../../../images/When-Date-Less-Than.png)

Activates the rule when a widget contains a date earlier than a set date, or earlier than a date in another widget.
