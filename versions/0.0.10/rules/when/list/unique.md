![](../../../images/When-List-Unique.png)

Activate the rule when a list widget doesn’t match any other list widget.
