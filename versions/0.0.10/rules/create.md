![Rules editor tab](../images/Rules%20Dropdown.png)

Creating rules is done under the “Rules Editor” dropdown tab in the top left corner of [Celerform Designer](/designer). ![](..../images/Blank%20Rules%20Engine%20Page.png) Select an existing widget in the form and click the “Create” button. ![](../images/Rule%20Create%20Button.png) The rule editor will automatically appear when you create a rule. ![](../images/Rule1.png) Here we can change the rule trigger, the rule title, add additional sources, and add the rule components that will build the rule. For more detailed information on these, check out [Rule Components]().

For this example rule, two sources will be required, so we’ll add a second source from the Additional Sources button. ![](../images/Additional%20Sources.png)

From here, select the rule component or set of components to create the rule. In this example, we’ll make an addition rule. To do this, we’ll start with a Number-Add rule component. ![](../images/Number-Add.png)

This will pull up the rule component editing box. ![](../images/Addrulebox.png)
Here we can select what widgets, values, or results to add together. We can select what from a dropdown menu. ![](../images/FromDataComponent.png)
From the Form Component Data component, a selection menu allows selection of the data components you want to add. ![](../images/FilledNumberAdd.png)

More components or static numbers can be added using the “plus” icon.
To view the result of the rule, an updated widget is needed. We’ll add a Generic-Update rule component to show our result. ![](../images/Generic-update.png) We create this by clicking the “Add” button. ![](../images/RuleAddButton.png)

Select the final result box from the Form Data Component dropdown for what to update, and select “Result From Previous Rule” as the update source. ![](../images/Update%20Dropdown.png) Finally, we’ll select the “Add” rule to specify which rule we’re updating from. ![](../images/Chain%20Add.png)

The final rule looks like this in the rule editor. ![](../images/Final%20Add%20Rule.png)

Since rules are updated automatically, you can now close the rule editor.
