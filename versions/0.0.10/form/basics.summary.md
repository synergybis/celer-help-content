<details><summary>Creating a form</summary>
On the [Celerform Designer](/designer) page there are several buttons beneath “Create a New Form.” Currently there are three form types, basic, grid and multi-page. ![](master\Versions\0.0.1\Sections\Building a Form\Forms.png) Clicking on one of these and selecting a form style, survey, save and resume, collaboration, or records style will create a new form.    For more information, see [Creating a Form]. </details>

<details><summary>Designing a form</summary>
Form design is based off of a simple drag and drop method. To create a form component, click the   symbol under “Data Components.” Choose your component type and drag and drop the component into either the Form Outline or the Form Preview. Read more about designing a form under [Form Design]. For more information on each data component, check out [Widgets].</details>

<details><summary>Importing a form</summary>
Celerform can import .pdf, .xsd, and .json files into a form. Create a form and click on the   import button under “Data Components.” This will open your file browser and allow you to import your form. The form will import as several data components that can be dragged into the form outline.</details>

<details><summary>Publishing</summary>
When you are finished creating a form, you can publish it at a unique url, which can then be sent to people to fill out your form. This is done under the  publishing menu. Forms can also have later versions published if you want to edit a published form. For more information, see [Publishing].</details>

<details><summary>Data storage</summary>
Data can be stored in save and resume forms, and in submissions. Save and resume forms will let you return to your form prior to submitting data without losing responses. See [Save and Resume]. Submitted responses can be exported as .csv or .json files. Entire forms can also be exported as .json files. For more information, see [Exporting Forms] and [Export] for exporting data.</details>