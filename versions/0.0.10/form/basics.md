On the [Celerform Designer](/designer) page there are several buttons beneath “Create a New Form.” Currently there are three form types, basic, grid and multi-page. ![](../images/Forms.png) Clicking on one of these and selecting a form style, survey, save and resume, collaboration, or records style will create a new form. ![](../images/Form_Styles.png) For more information, see [Creating a Form].

### Designing a form

Form design is based off of a simple drag and drop method. To create a form component, click the ![](../images/Add_Icon.png) symbol under “Data Components.” Choose your component type and drag and drop the component into either the Form Outline or the Form Preview. Read more about designing a form under [Form Design](). For more information on each data component, check out [Widgets](../widgets).

### Importing a form

Celerform can import .pdf, .xsd, and .json files into a form. Create a form and click on the ![](../images/Import_Icon.png) import button under “Data Components.” This will open your file browser and allow you to import your form. The form will import as several data components that can be dragged into the form outline.

### Publishing

When you are finished creating a form, you can publish it at a unique url, which can then be sent to people to fill out your form. This is done under the ![](..../images/Share_Icon.png) publishing menu. Forms can also have later versions published if you want to edit a published form. For more information, see [Publishing](/celer-help-content/versions/0.0.1/admin/publish.md).

### Data storage

Data can be stored in save and resume forms, and in submissions. Save and resume forms will let you return to your form prior to submitting data without losing responses. See [Save and Resume](persist.md). Submitted responses can be exported as .csv or .json files. Entire forms can also be exported as .json files. For more information, see [Exporting Forms] and [Export] for exporting data.

### Multiple page forms

Wizard style forms allow users to create forms with multiple pages. Creating a default or government style form gives a dropdown allowing for either survey or wizard style forms to be selected. Adding a container to a wizard form in Celerform Designer will add a new page to the wizard form.
