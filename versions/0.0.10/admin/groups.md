In [Celerform Administration](/admin) under Organizational Information there are three tabs, users, groups, and roles. The groups tab will allow you to create and manage groups.

### Creating a New Group
Under the [Groups] tab you can edit current groups and create new groups. Groups can be named and assigned roles to create custom groups of permissions.

### Managing Groups
Groups are managed by adding and removing users to groups. To add or remove users, select the user under the [Users] tab, and add or remove them from any available group. Like users, groups can be given editing, submission, reviewing and admin permissions under the Users tab in the [Publishing menu] for individual forms.