## Published Forms

The publishing menu is available for any published form under [Celerform Administration](/admin) under [View Forms](/admin/forms). The publishing menu for a form includes several tabs that allow you to change form types and versions, retrieve submission data, and change form users and settings.

### Submissions
Submissions can be viewed and deleted from the submissions tab under the published form menu. Submissions can also be analyzed by responses under the Analytics tab. Finally, submission data can be exported as a .json or .csv file under the Export tab. Json exports can either export just the submission data, or all data including metadata. CSV exports will only export submission data, without metadata.

### Versions
When publishing a form, the form creates a new version of itself. Under the versions tab, you can switch the different active version of a form, without deleting new versions. This is useful when you want to roll back a change to a form.

### User Authentication 
Under settings administrators can choose whether or not users must log into Celerform to fill out a form by activating user authentication. By default, user authentication is active for save and resume, collaboration, and record style forms, and inactive for survey style forms. Survey forms are recommended not to use authentication, since most survey responders will not need Celerform accounts, but it is generally useful for the other form types. When user authentication is active, users can be assigned as submitters or reviewers for forms, which will limit who can submit to the forms. For more information, view [Collaboration Forms](/docs/0.0.10/runtime/collab).