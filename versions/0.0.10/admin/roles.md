In [Celerform Administration](/admin) under Organizational Information there are three tabs, users, groups, and roles. The roles tab will allow you to create and manage roles. Roles function as permissions within Celerform.

### Creating a New Role
Under the [Roles] tab you can edit current roles and create new rules. Currently, the only property of roles you can edit is their names, but more options will be added in future updates. System roles are default roles built into Celerform. These cannot be edited, unlike user created roles.

### Managing Roles
Roles can be managed by assigning groups or users roles. Individual users can be assigned roles by assigning groups or individual roles under the [Users] tab.