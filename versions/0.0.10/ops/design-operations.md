**Form Creation**

New forms can be created in Celerform Designer by clicking one of the three form icons at the top of the page. Three different form types can be selected, survey, grid, and multi-form. Survey forms are simple forms that have one element per line. Grid forms use grid widgets by default, and are designed to use grid row containers to create rows of elements. Multi-page forms include a navigation widget at the top. Containers can be added to the navigation widget, which allows navigation directly to that part of the form. Form types can be changed under settings in Celerform Designer.

**Form Uploading**

Forms can be uploaded directly to Celerform Designer in .json file format.

**PDF Imports**

PDF files can be imported into a form in Celerform Designer. PDFs import as a set of data components that can be dragged into the form to recreate the PDF.

**XSD Imports**

XSD files can be imported into a form in Celerform Designer. XSDs import as a set of data components that can be dragged into the form to recreate the XSD.

**API Imports**

API files can be imported into Celerform to link forms to external databases.

**Form Editing**

Forms can be edited in Celerform Designer via widgets and rules. Widgets can be added to forms by dragging and dropping them. Widgets can also be added by importing pdf or json files into designer. Widget properties can be edited and have rules attached to them to edit how users interact with forms.

**Editing Widgets**

Widget names, descriptions, and types can be changed under the widget’s Visual tab.

**Removing Widgets**

Widgets can be removed through Celerform Designer’s delete button.

**Disabling Widgets**

Widgets can be disabled through Celerform Designer, under the widget’s Data tab, or by creating a disabling rule.

**Hiding Widgets**

Widgets can be hidden through Celerform Designer, under the widget’s Data tab, or by creating a rendering rule.

**Copying Widgets**

Widgets can be duplicated in Celerform Designer for quick form building.

**Adding Tooltips**

Tooltips can be added to widgets under the widget’s Visual tab.

**Changing Data Pathways**

Widget data paths can be changed under the widget’s Data tab. Note that no two widgets can have identical data paths.

**Placeholders and Default Values**

Widgets can be given default values, which will appear in the form when first loaded. Widgets can also be given placeholders that will appear until data is typed in. Both of these options are available under the Visual tab.

**Rows and Columns**

Rows of widgets can be made using the “Grid Row” container widget in grid style forms. The number of columns in a widget can be controlled with the columns option under the Visual tab, or by dragging and dropping new widgets into the grid row.

**Multiple Pages**

Wizard style forms can contain multiple pages by adding containers. Each container in the form editor will act as a new page.

**Requiring Values**

Widgets can be set to be required under the Data tab. Forms will not submit until all required widgets have been filled out.

**Min and Max Values**

Widgets can be set to have required minimum and maximum values, these settings can be found under the Data tab.

**Custom Error Messages**

Widgets can have custom error messages attached to them through the Data tab. These can be custom messages for if a value is required, or if a value has gone below a minimum or over a maximum value.

**Form File Uploads**

Files can be attached to forms through the attachment widget.

**Document Signing**

Forms and documents can be signed using the signature widget.

**Date Searching**

All date widgets come with a date picker to select the date from a calendar.

**Grids**

Celerform can create pdf-style grid forms using the grid-style forms.

**Secure Widgets**

Social security number and blurred text widgets allow for data to be invisible to unauthorized users.

**Logic Rules**

Celerform Designer contains a rules engine that can create logic rules. Rules can perform a number of functions that are covered in greater detail below.

**Rule Triggers**

Rules are activated and deactivated by rule triggers, which is usually interaction with a widget, or the form being loaded.

**Rule Sources**

Rules that are triggered by user interaction need sources to activate them. Multiple sources can be used for the same rule.

**Rule Conditions**

Rules can be set to trigger under certain conditions, such as a date being within a certain timeframe, or whether a checkbox is checked. Types of rule conditions are discussed below.

**Equals Conditions**

Rules with this condition will only activate when two or more data equal each other. These data can include form data, or pre-set static data.

**Empty Conditions**

Rules with these conditions will activate when a widget is empty.

**Filled Conditions**

Rules with these conditions will activate when a widget contains data.

**Relative Date Conditions**

Rules with these conditions will activate when a date falls before or after a certain date or time period. Dates can also be required to be within a certain timeframe to activate these rules.

**Relative Number Conditions**

Rules with these conditions activate when a number is greater or less than another number. This can be compared to other number data, or pre-set static numbers.

**Rule Features**

Every rule affects the form in some way when activated or deactivated. The various types of rule features are discussed below.

**Updating Rules**

Rules can update or clear data within a form.

**Rendering Rules**

Widgets can be hidden or shown using rules.

**Disabling Rules**

Widgets can be enabled or disabled by rules.

**Mathematical Rules**

Basic math operations including addition, subtraction, multiplication and division are available in the Celerform rules engine.

**String Rules**

Strings of characters can be combined using rules.

**Endpoint Rules**

Celerform rules can interact with databases to look up information, including ZIP codes, cities and addresses.

**Publishing**

Celerform forms can be published from designer as interactable forms on Celerform runtime. Published forms are accessible to anyone with the link, or, if user authentication is required, the form is accessible to any user assigned to the form.

**Custom Submissions Page**

The submission page for a form can be altered under settings in designer.

**Secure Submissions**

Data can be securely submitted in Celerform, only allowing admins or specific users read access.