**Surveys**

Celerform can create and share surveys to survey participants.

**Multi-page/wizard-style forms**
Celerform can create forms with multiple pages using the wizard style form.

### Data Saving

Celerform can save data written into unsubmitted forms for save and resume style work.

### Submission Review

Collaboration forms allow assigned users to review and accept or reject submissions. Submissions can also be partially rejected, the reviewer only rejects the form components that are incorrectly filled out, and the submitter can only edit those rejected components. Assign users as submitters and reviewers in Celerform Administration. Only submitters who have been assigned collaboration forms can see the forms in runtime, and only reviewers assigned to the form can see the submissions for review.

### Records

Records can be created and maintained in Celerform with records style forms. Each form submission will save as a record within the form. Records can be updated or removed from the Celerform database.