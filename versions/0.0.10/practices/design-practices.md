### Creating a default form
1)	Click the Default Form button on the Celerform Designer homepage.
2)	Select whichever style best suits your form in the form creation window: survey, save and resume, collaboration, or record.
3)	Type in the name of your form in the Form Namespace.
4)	Click the Start Building button.
5)	Create widgets using Data Components and add widgets from Data Components to your Form Outline.
6)	Select the Share button to bring up the Publish menu, select the Publish button.
7)	Share your form link to survey participants.

### Creating a grid form
Creating a grid form is done with the grid form type.
1)	Click the Grid Form button on the Celerform Designer homepage.
2)	Select whichever style best suits your grid form in the form creation window: survey, save and resume, collaboration, or record.
3)	Type in the form’s name in the Form Namespace.
4)	Click the Start Building button.
5)	Create widgets using Data Components.
6)	Create container items using the Form Outline. These will automatically default to Grid Rows.
7)	Add your Data Components to your Grid Rows by dragging and dropping them into the rows.
8)	Select the Share button to bring up the Publish menu, select the Publish button.
9)	Share your form link to assigned users or survey participants.

### Creating a government form
1)	Click the Government Form button on the Celerform Designer homepage.
2)	Select the style that best suits the form in the form creation window.
3)	Type in the form’s name in the Form Namespace.
4)	Click the Start Building button.
5)	Create widgets using Data Components.
6)	Select the Share button to bring up the Publish menu, select the Publish button.
7)	Share your form link to assigned users or survey participants.

### Creating a survey style form
1)	Click the button corresponding to the desired form type: Default,Grid, or Government on the Celerform Designer homepage.
2)	Select the Survey Style button in the form creation window, this should be selected by default.
3)	Type in the name of your form in the Form Namespace.
4)	Click the Start Building button.
5)	Create widgets using Data Components and add widgets from Data Components to your Form Outline.
6)	Select the Share button to bring up the Publish menu, select the Publish button.
7)	Share your form link to survey participants.

### Creating a save and resume form
Any type of form can be made into a save and resume style form, using the form creation window.
1)	Select the button, default, grid, or government, that works best for your type of form on the Celerform Designer homepage.
2)	Select the Save and Resume style button in the form creation window.
3)	Type in the form’s name in the Form Namespace.
4)	Click the Start Building button.
5)	Create widgets using Data Components.
6)	Add your Data Components to the Form Outline to construct your form.
7)	Select the Share button to bring up the Publish menu, select the Publish button.
8)	Share your form link to assigned users or survey participants.

### Creating a collaboration form
Any type of form can be made into a collaboration form. Note that collaboration forms require users to be assigned as submitters and reviewers from Celerform Administration.
1)	Select the survey, grid, or multi-page button that works best for your form on the Celerform Designer page.
2)	Select the Collaboration Workflow button in the form creation window.
3)	Type in the form’s name in the Form Namespace and click the Start Building button.
4)	Create your form using the form editor.
5)	Select the Share button to bring up the Publish menu, select the Publish button.
6)	Go to your form’s page under View Forms in Celerform Administration. If you don’t have access to Celerform Administration, request an Administrator to perform the following steps for you.
7)	Go to the Users tab in your form’s settings. Note that this is the published form’s users tab, not the organization’s users tab, which is for creating and deleting users.
8)	Select the available users that you want to have submit or review the form from the Available Users dropdown menu.
9)	Assign submitter and reviewer roles to users. Submitters can create new submissions, and reviewers can check them over and either approve or reject them.
10)	Send your form’s link to your submitters. They can they fill out the form, and reviewers will accept or reject their submissions.

### Creating a records style form
Any type of form can be used as a records form. Note that unless altered in Celerform Administration, records forms require users to be signed in to use them.
1)	Select the survey, grid, or multi-page button that works best for your records form on the Celerform Designer homepage.
2)	Select the Records Style button from the form creation window.
3)	Type in the form’s name in the Form Namespace and click the Start Building button.
4)	Create your form using the form editor.
5)	Select the Share button to bring up the Publish menu, select the Publish button.
6)	Go to your form’s link to view the form’s record homepage.
7)	The record homepage will show you all saved record submissions. Click the plus icon to create a record.
8)	A copy of your form will appear that can be filled out, saved, and edited as a record.
9)	Records can be deleted using the trash icon. Switch between records by clicking them from the homepage, or using the arrow icons.
10)	When you’re done editing your record, click the save icon. Records can be edited after being saved.

### Creating a wizard form
1)	Click the button corresponding to the desired form type, Default or Government, on the Celerform Designer homepage. (Note grid forms aren't able to be turned into wizard forms.)
2)	Select the style that best suits your form: survey, save and resume, collaboration, or record.
3)  Select Wizard Form from the Form Widget dropdown.
4)	Type in the name of your form in the Form Namespace.
5)	Click the Start Building button.
6)  Click the add icon under Data Components and select Container. Drag and drop the container into the Form Outline. This will create a container that functions as the first page for the form.
7)	Create widgets using Data Components and add widgets from Data Components to your container.
8)  Create another container by dragging and dropping the container data component into the Form Outline. This will be the second page of the form. A page break line will appear between the two containers.
9)  Add widgets to the second container to fill your second page.
10) Continue creating new containers and adding widgets to create as many pages as are needed for the form.
11)	Select the Share button to bring up the Publish menu, select the Publish button.
12)	Share your form link to survey participants.

### Editing a form
Both published and unpublished forms are automatically saved and stored in Celerform Designer. Forms can be edited from designer, but must be re-published to update the form.
1)	Select the form you want to edit.
2)	The form editor will now appear, make your edits to whichever widgets you want. You can also add new widgets from Data Components, or remove unwanted one with the Form Outline’s delete button.
3)	Click the Share button to bring up the Publish menu.
4)	Click the Activate form on publish checkbox if you want to immediately update the form. If this is not checked, a new, inactive version of the form will be created, that an administrator can later activate.
5)	Click the Publish button.
6)	The updated version of the form is now published and can be accessed as long as it is active.

### Creating and adding components
Widgets, or components, comprise any created form in Celerform. Creating and adding widgets is done through drag-and-drop in the form editor.
1)	Select the add icon under Data Components. This will show a drop-down list of various component types you can add to your form.
2)	Select the type of component you want to add to your form, it will then appear under Data Components.
3)	Drag and drop your data component into the Form Outline or directly into the Form Preview. The component will now appear as a widget in your form. Note that one data component can be used to create multiple widgets in the form.
4)	You’re now free to edit your widget as you like. Note that identically named widgets should be placed into different container components, or have different data paths. Containers and data paths are covered later in this document.
5)	Labels, containers, progress bars and navigation widgets can all be accessed from the add icon under Form Outline. These components are considered visual components and don’t contain much, if any, data. Simply clicking the add icon and selecting the visual component you want will add it to the Form Outline.

### Removing widgets
Widgets can be deleted in the Form Outline in the form editor.
1)	Select the widget you want to remove from your form.
2)	Click the trash icon under Form Outline. This will remove your widget from the form.

### Removing data components
Data components can be removed from Data Components without affecting the form.
1)	Select the data component you want to remove.
2)	Click the trash icon under Data Components. This will remove the data component without affecting the widgets in the form. Note that since removing data components doesn’t directly affect the form in most cases, this is mostly just for organizational purposes for the form creator.

### Importing PDFs
PDF, JSON and XSD files can all be imported into Celerform Designer to recreate forms in the Celerform format.
1)	Create a new form in Celerform Designer by selecting the form type, the form style, and naming your form in the form creation window.
2)	Click the Start Building button.
3)	Click the import icon under Data Components.
4)	Click the Import PDF button and select the pdf file you want to import.
5)	The pdf will import as a set of Data Components within a pdf container. Expand the container by clicking the caret down icon on the container and drag the pdf components into the Form Outline.
6)	Continue dragging components into the Form Outline to reconstruct your pdf file.
7)	Click the Share button to open the Publish menu and click the Publish button.
8)	Your pdf is now a published form, share the link to assigned users or survey submitters to fill out the form.

### Importing XSDs
PDF, JSON and XSD files can all be imported into Celerform Designer to recreate forms in the Celerform format.
1)	Create a new form in Celerform Designer by selecting the form type, the form style, and naming your form in the form creation window.
2)	Click the Start Building button.
3)	Click the import icon under Data Components.
4)	Click the Import XSD button and select the xsd file you want to import.
5)	The xsd will import as a set of Data Components within a xsd container. Expand the container by clicking the caret down icon on the container and drag the xsd components into the Form Outline.
6)	Continue dragging components into the Form Outline to reconstruct your xsd file.
7)	Click the Share button to open the Publish menu and click the Publish button.
8)	Your xsd is now a published form, share the link to assigned users or survey submitters to fill out the form.

### Importing JSON files
PDF, JSON and XSD files can all be imported into Celerform Designer to recreate forms in the Celerform format.
1)	Create a new form in Celerform Designer by selecting the form type, the form style, and naming your form in the form creation window.
2)	Click the Start Building button.
3)	Click the import icon under Data Components.
4)	Click the Import JSON button and select the json file you want to import.
5)	The json will import as a set of Data Components within a json container. Expand the container by clicking the caret down icon on the container and drag the json components into the Form Outline.
6)	Continue dragging components into the Form Outline to reconstruct your json file.
7)	Click the Share button to open the Publish menu and click the Publish button.
8)	Your json is now a published form, share the link to assigned users or survey submitters to fill out the form.

### Renaming widgets
Widgets can be given a new name in the form editor.
1)	Select the widget you want to rename. Widgets in both the Form Outline and in Data Components can be renamed.
2)	Click the element title textbox. Remove the current widget name from the textbox, and type the new name into it.
3)	Click off the element title textbox. Your widget will automatically update the new name from the textbox.
Adding widget descriptions
Widgets can be given a description in the form editor.
1)	Select the widget you want to give a description. Widgets in either the Form Outline or in Data Components can be given new descriptions.
2)	Select the description textbox and type in the description you want to add.
3)	Click off the description textbox, the widget will automatically update with the new description.

### Hiding widgets
Widgets can be hidden in the form editor. This can be useful in combination with rendering rules if you only want a widget to be shown if another widget has been filled.
1)	Select the widget you want to hide.
2)	Select the hidden checkbox under the visual tab.
3)	The widget will now be hidden, uncheck the hidden checkbox if you want to reveal the widget. Hidden widgets will still be visible in the Form Outline so that they are editable.

### Disabling widgets
Widgets can be disabled in the form editor. This is most useful when combined with rules, such as disabling two widgets when they are equal.
1)	Select the widget you want to disable.
2)	Select the disabled checkbox under the visual tab.
3)	The widget will now be disabled, uncheck the disabled checkbox if you want to enable the widget.

### Changing widgets
Data components and elements can be changed into different widgets of the same element type.
1)	Select the widget you want to change.
2)	Select the widget dropdown menu.
3)	Select the new widget that you want to use.
4)	The widget will automatically update once selected to the new widget. For a list of widgets, check out widgets under Celerform Help.

### Changing types
Data components and elements can be changed into a different widget type.
1)	Select the widget you want to change.
2)	Select the change type icon under the type label in the visual tab.
3)	Select the type of component you want to switch to. If the type you want to change to isn’t visible, select show all types.
4)	The widget will automatically update to the new type you have selected.

### Duplicating widgets
Widgets can be duplicated in the form editor using either the duplicate or copy and paste functions.
For duplication:
1)	Select the widget you want to duplicate.
2)	Select the duplicate icon under Form Outline.
3)	A duplicate widget will now appear at the bottom of the Form Outline.

For copy and paste:
1)	Select the widget you want to copy.
2)	Select the clipboard icon under Form Outline.
3)	The widget will now be stored on the clipboard. When you want to paste in a copy of the widget, click the paste icon, and a copy will appear at the bottom of the Form Outline.

### Selecting multiple widgets
Multiple widgets can be selected and edited simultaneously. This is useful if you want to duplicate or copy multiple widgets at the same time.
1)	Select the multi-select icon under either Form Outline or Data Components. Both data components waiting to be added and existing widgets can be selected.
2)	Checkboxes will appear next to the widgets or data components. Select the checkboxes next to the components you want to select.
3)	Edit the selected components how you want to. When you want to deselect the components, either click their checkboxes, or click the multi-select icon to exit multi-select mode.

### Adding widgets to a container
Widgets can be grouped into containers for organizational purposes or to put them into grid rows.
1)	Create a container component by selecting the add icon under Form Outline and clicking Container. Alternatively, containers can be created as data components and dragged into the form outline.
2)	Create the component types you want to add in Data Components by selecting the add icon.
3)	Drag and drop your data components into the container. Note that you can add as many components as you like to most containers. Grid row containers put all components in a row, so it is possible to overflow your row in your form, we recommend adding no more than 8 components to a single row.
4)	Containers don’t necessarily need to have names, but if they do have a unique data path, they can contain identical widgets in separate containers.

### Adding columns to a grid row
Multiple widgets can be added to a grid row container widget. Each widget takes up one column by default. The number of columns can be altered under the visual tab.
1)	Create a container with the grid row widget selected from the widget dropdown.
2)	Add widgets to your grid row via drag and drop. New widgets will automatically add one column to the grid row.
3)	If there are too many or too few columns, scroll down to the columns section of the visual tab. Each column is represented by a green box, and the number can be adjusted by clicking on the plus or minus icons. Alternatively, you can click the box that corresponds with the number of columns you want in the row.
4)	Changing the number of columns a widget takes up will expand or contract the widget. By default, each widget takes up one column space, but widgets can be adjusted to take up more columns.

### Choice widgets
Choice widgets are a type of data component that can be added to forms. Here we’ll go over adding choices to your choice widgets. Multi-select, choice description and complex choice field widgets all allow for multiple choices to be selected, all other choice widgets only allow for one choice to be submitted.
1)	Create a choice data component by clicking the add icon under Data Components and clicking the choice data component. Drag and drop the data component into the Form Outline or Form Preview.
2)	Scroll to the choice options section of the visual tab on the right-hand side. Type in the first choice into the available textbox and hit enter or the plus icon next to the textbox.
3)	Continue adding choices until all your choices are available. Choices can be edited by editing the text in their textboxes, or removed by clicking the cross icon next to the choices.

### “Other” widget types
Most widgets such as signature blocks or text boxes don’t require any special editing in the form editor to function correctly. “Other” widget types are a unique set of widgets that include rating fields and coordinate fields. Creating these as widgets or data components is done as any other, by choosing them from Data Components and adding them to the Form Outline or Form Preview. Rating fields will appear as 5 empty clickable stars. Coordinate fields act like number fields, but will only accept valid latitude or longitude coordinates.

### Lists
Lists are containers that do not immediately show the widgets inside of them. List can contain any number of widgets or even other containers. To show the widgets inside of a list, click the add button on the list. This will add a copy of all the widgets in the list to the form. Lists can add as many copies of the widgets contained within as the submitter requires. Excess copies of lists can be removed by clicking it’s respective remove button.

### Changing data paths
Data paths are unique names that tell Celerform where to store data. Data paths use the widget’s name by default but can be customized for easier data sorting. Note that no two data paths can be identical, and forms with identical paths will not publish. Having two identical widgets can have separate data paths by either changing one of the paths manually, or by placing them in separate containers that have different data paths.
1)	Select the widget that you want to change the data path of.
2)	Click on the Data tab in the top right-hand corner. This will show you more advanced data-related information about the widget.
3)	Clear the path textbox and type in the new data path. Note not to enter any special characters including commas or periods into the data path, as these can make data storage unnecessarily complicated.
4)	If two data paths are identical, Celerform will produce a duplicate absolute path error, letting you know which components are identical.

### Required widgets
Form components can be required to be filled out before forms can be submitted.
1)	Select the widget you want to have be required.
2)	Click on the Data tab in the top right-hand corner.
3)	Check the required checkbox, this will place an asterisk on the widget, indicating that it is required before the form can be submitted.
4)	You can also type in a custom error message into the custom error message textbox just below the required checkbox. This custom error message will pop up instead of the default message when the form is submitted without the widget filled out.

### Minimum and maximum values
Celerform widgets can be given minimum and maximum character lengths as responses.
1)	Select the widget you want to add a minimum or maximum character length to.
2)	Click the Data tab on the top right-hand corner of the screen.
3)	Enter minimum values into the minimum value – value textbox and maximum values into the maximum value – value textbox.
4)	You can enter custom error messages that will override the default messages if the value is too short or too long in the minimum value – custom error message and maximum value – custom error message textboxes.

### Widget tooltips
Widgets can have tooltips attached to them to give more information or help to form submitters.
1)	Select the widget you want to add a tooltip to.
2)	Scroll down to the tooltip textbox. Type in the tooltip that you want to have appear for the widget.
3)	A small icon will now appear next to your widget. Hovering the mouse over this icon will make the tooltip text pop up.

### Widget placeholder text
Widgets can be given placeholder text that will be visible until something is typed into the widget.
1)	Select the widget you want placeholder text for.
2)	Scroll down to the placeholder text textbox. Type in the placeholder text you want to have shown.
3)	The placeholder text will appear in your widget as greyed out text as long as the widget is empty.

### Widget default value
Widgets can be given a default value. Default values will appear in widgets when the form loads, instead of the widget appearing as blank.
1)	Select the widget you want to add a default value to.
2)	Scroll down to the default value textbox and type in the default value.
3)	When the published form is loaded, the widget will have the default value already typed into it. The default value can be changed by the submitter, or left as is.

### Subtitled widget
Widgets can be set so that their titles appear as subtitles.
1)	Select the widget you want to have appear as a subtitle.
2)	Scroll down to the subtitle checkbox and click it.
3)	The title of the widget will now appear as a subtitle instead of as regular text.