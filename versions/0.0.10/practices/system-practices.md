### Undoing a published edit
Published edits can be reversed by activating a previously published version in Celerform Administration.
1)	Visit the form’s page under View Forms in Celerform Administration.
2)	Click the Versions tab.
3)	Select the version that you want to activate. Every updated version of the form since the form’s creation is stored here and can be reactivated.
4)	Click the menu button on the right side of the version you want to activate and click Activate Version.
5)	The version you have selected is now active in Celerform.

### Viewing Submissions
Submissions for forms can be viewed under Celerform Administration.
1)	Visit the form’s page under View Forms in Celerform Administration.
2)	Click the Submissions tab.
3)	Select the submission you want to view. Submissions are automatically sorted from oldest to newest.
4)	The submission data will now appear, it can be viewed either as responses to questions as shown in the form, or as raw data under the Data tab. If files have been attached to the submission, they can be viewed under the Attachment tab. Note that if no attachments have been uploaded, this tab will not appear.

### Removing submissions
Unwanted or erroneous submissions can be deleted from Celerform Administration.
1)	Visit the form’s page under View Forms in Celerform Administration.
2)	Click the Submissions tab.
3)	Select the submission you want to delete and click the corresponding menu icon on the right. Submissions automatically sort from oldest to newest.
4)	Select the Delete button from the dropdown menu. This will remove the corresponding submission. We recommend double checking to make sure that you are deleting the correct submission, since there currently is no recycle bin for submissions.

### Changing a published form’s style
Published forms can switch between survey, save and resume, collaboration workflow, and records form styles under Celerform Administration.
1)	Visit the form’s page under View Forms in Celerform Administration.
2)	Click the Settings tab.
3)	Select the form style you want to update the form to. Note that if you select Records Style or Collaboration Workflow that you will have to enable user authentication on the same page and assign users under the Users tab.

### Viewing analytics
Form submission analytics can be viewed in Celerform Administration. Analytics provide an aggregate of data and submission information for individual forms.
1)	Visit the form’s page under View Forms in Celerform Administration.
2)	Click the Analytics tab.
3)	Analytics for the currently active version of the form will automatically appear. Previous versions’ analytics can be viewed from the dropdown menu on the left. Analytics can be exported via the export icon on the right or printed with the print button next to it. Analytics can also be updated live with the live reload function in the middle of the top of the page.

### Exporting submission data
Form submission data can be exported as either .csv or .json files through Celerform Administration.
1)	Visit the form’s page under View Forms in Celerform Administration.
2)	Click the Export tab.
3)	Click one of the three export buttons, depending on what type of data you want. Full export will export all data, including question information as a .json file. Export data only will export response data, but not question information as a .json file. Export data only as csv will export response data, but not question information as a .csv file. The exported file will then download onto your local computer and can be shared and edited as any other .json or .csv file.

### Creating users
New user accounts can be created in the Users tab under Celerform Administration.
1)	Click the Users tab on Celerform Administration.
2)	Click the Create User button at the top right of the page.
3)	Fill out the user’s information: first name, last name, email address and username.
4)	Type in a temporary password or copy the automatically generated password. Send this password to your user, they will need it to log in for the first time. Upon first logging in Celerform will prompt the user to create a new password.
5)	Assign any groups or roles to the user.
6)	Click the Add User button.

### Editing users
Users can be edited from the Users tab.
1)	Click the Users tab on Celerform Administration.
2)	Click the user you want to edit.
3)	Edit the user how you like, multiple options can be edited, including first and last name, email address and username. Remove or assign roles or groups to the user. User passwords can be reset by clicking the Reset Password button, which will send them a link to reset their password at their email.
4)	Click the Save Edit button when all your edits are done.

### Removing users
User accounts can be deleted when no longer needed.
1)	Click the Users tab in Celerform Administration.
2)	Click the menu icon on the right-hand side of the user you want to remove.
3)	Select the Delete button from the dropdown menu.
4)	We recommend double checking to make sure that you are deleting the correct user.

### Creating roles
Roles are sets of permissions that can be assigned to users. New roles combining different default permissions can be created in Celerform Administration.
1)	Click the Roles tab in Celerform Administration.
2)	Click the Create Role button in the top right of the window.
3)	Name the role and assign the various permissions from the dropdown menu.
4)	Click the Create Role button.

### Editing roles
Roles can have their names and permissions edited.
1)	Click the Roles tab in Celerform Administration.
2)	Click the role you want to edit.
3)	Change the role’s name or the various permissions attributed from it from the dropdown menu.
4)	Click the Save Edit button when all edits are finished.

### Deleting roles
1)	Click the Roles tab in Celerform Administration.
2)	Click the menu icon on the right-hand side of the role you want to remove.
3)	Click the Delete button from the dropdown menu.

### Creating groups
Groups are sets of users with the same set of assigned forms and permissions. Users can be part of multiple groups to assign them to different projects.
1)	Click the Groups tab in Celerform Administration.
2)	Click the Create Group button in the top right of the window.
3)	Name the group and assign roles or permissions from the dropdown menus.
4)	Click the Create Group button
5)	Assign users to the group through the Users tab. For more information, see Editing users.

### Editing groups
1)	Click the Groups tab in Celerform Administration.
2)	Click the group you want to edit.
3)	Edit the name or permissions of the group using the dropdown menu and name textbox.
4)	Click the Save Edit button when all edits are finished.

### Deleting groups
1)	Click the Groups tab in Celerform Administration.
2)	Click the menu icon on the right-hand side of the group you want to delete.
3)	Click the Delete button from the dropdown menu.