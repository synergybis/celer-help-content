In [Celerform Administration](/admin) under Organizational Information there are three tabs, users, groups, and roles. The roles tab will allow you to create and manage roles.

<details><summary>Creating a New Role</summary>
Under the [Roles] tab you can edit current roles and create new rules. Currently, the only property of roles you can edit is their names, but more options will be added in future updates.</details>

<details><summary>Managing Roles</summary>
Roles are managed by adding and removing users. Adding and removing users can be done by clicking on the user under the [Users] tab, and adding or removing them from the selected role. Roles can be given editing, submission, reviewing and admin permissions under the Users tab in the [Publishing menu] for individual forms.</details>