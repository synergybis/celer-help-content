In [Celerform Administration](/admin) under Organizational Information there are three tabs, users, groups, and roles. The users tab will allow you to create and manage users.

### Creating Users
Click on the [Users] tab on the left side of the screen. On the top right of the users tab, click Create User. This will take you to a page where you can fill out the user’s information, assign them to [Groups] and [Roles], and create a temporary password for them.

### Managing and Editing Users
Once a user has been created, users can be edited by clicking on their usernames in the [Users] tab. Here you can change a user’s name, their email, username, and assign roles and groups to them. Additionally, administrators can reset a user’s password here in the event a user forgets their login credentials.