In [Celerform Administration](/admin) under Organizational Information there are three tabs, users, groups, and roles. The groups tab will allow you to create and manage groups.

<details>
<summary>Creating a New Group</summary>
Under the [Groups] tab you can edit current groups and create new groups. Currently, the only property of groups you can edit is their names, but more options will be added in future updates.</details>

<details>
<summary>Managing Groups</summary>
Groups are managed by adding and removing users to groups. To add or remove users, select the user under the [Users] tab, and add or remove them from any available group. Groups can be given editing, submission, reviewing and admin permissions under the Users tab in the [Publishing menu] for individual forms.</details>