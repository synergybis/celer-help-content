## Saving Data
<details><summary>Save and Resume</summary>
Save and resume style forms will automatically save any data entered, allowing you to exit the form and return to it without losing data. These can be created by selecting “Save and Resume” form style   when creating a form, or by changing to a save and resume style under form settings in Celerform Administration. For more information on Save and Resume forms, visit [Form Types](/form_types).</details>

<details><summary>Exporting Forms</summary>
The settings   menu in the top left of Celerform Designer allows you to export your form as a .json file, which can then be re-imported into Celerform later or into any other program that accepts .json formats.</details>

<details><summary>Records Forms</summary>
Records forms are designed to save multiple records to local storage, or to another application via API. Record forms are created by selecting “Records Style”   when creating a form, or by changing to a records style under form settings in Celerform Administration. For more information on records forms, visit [Form Types] or [APIs].</details>