When you want to start a new form, you can create it on the Homepage using the quick start (above as well) or using the icon within the header bar. gives you. New Form Page gives you more options up front on the design and layout of your form.

## Form Style

Changes the workflow of the form, depending on what type of form you are creating.

## Using Celerform Templates

If you have a Celerform template, you can import the file on the Import Form Page The only configuration allows is a new title. Using a template your form style and form type will remain the same as the initial form it is based on.

## How do I get a Celerform Template File?

An Celerform template file is going to be exported from an existing form. All Form actions, including exporting a form config s located within the settings panel. This is accessible by clicking the Cog settings icon in the Header of the Form Designer.