Celerform has three different form types which can each use 4 different form styles, making for a total of 12 different form combinations. The three form types are basic form, grid form, and multi-page form. The four different form styles are survey style, save and resume style, collaboration workflow, and records style.

### Default Form
The default form is the simplest type of form and lets you create basic forms. These forms utilize basic widgets that aren't suited for grids. Default forms can be used for wizard forms. The majority of forms will likely be best suited as default forms.

### Grid Form
Grid forms are used to create forms with rows and columns. They are created using Grid Row container components. Each grid row container functions as a row for the grid. Placing a component in the grid row will place the component in its own column. Since each grid row is its own component, each row can have a unique number of columns. Grid forms are best suited for pdf-style forms. Note that grid forms cannot be wizard forms.

### Government Form
Government style forms are similar to default forms, but use government-style widgets that meet all government organizational form requirements. Government forms can be used for wizard forms.

### Survey Form
Survey forms are the default type of form, and are comprised by a single page. Survey forms can be as long or as short as necessary. Any style of form can function as a survey form, and survey forms can use the rules engine.

### Wizard Form
Wizard forms are similar to survey forms, but are comprised of multiple pages. In designer, adding a container to the form will create a new page. Widgets placed within the container will pop up on the respective page. To make it clear what page is being used, page break lines appear between containers. When published, wizard forms have previous and next buttons to allow users to navigate between the form pages. First and last buttons can also be added in Celerform Designer under the settings menu. Wizard forms are only accessible for Default and Government style forms.

### Survey Style
Survey Style forms are the simplest form style. These forms will accept submissions when the submission button is clicked. They do not require a Celerform login by default, and can accept multiple submissions from the same submitter. These are best used for short surveys or basic forms.

### Save and Resume Style
Save and Resume forms are like Survey Style forms, except that they will retain information placed into them prior to submission. If needed, users can refresh the form’s page and their information will remain in the form. These are best used for long forms that require a lot of information.

### Collaboration Workflow
Collaboration Workflow forms are forms that require validation by a reviewer. Users must be assigned as either submitters or reviewers to this type of form. Once assigned, submitters can fill out and submit forms for review in Celerform Runtime, and their responses will be sent to reviewers. Reviewers can then go to Celerform Runtime and review responses. Reviewers can either accept or reject responses, and can mark individual components as accepted or rejected. If a submission is rejected, it is returned to the submitter to edit. If a submission is accepted, the submission will go to the submission section of the form’s administration page. These are best used for document information submission, or other information that demands high accuracy.

### Records Style
Records style forms are forms that keep copies of all entered submissions in a records folder. Unlike all other form types, following the link for a record form brings up the archive of records, or submissions, instead of the form itself. Records can then be viewed, or they can be added by clicking the   add button. Record forms will then contain a toolbar at the top of the form, which allow the user to view the previous or next record  , save the record  , add a new record  , delete the record  , reset the record  , or return to the form’s home archive  . Records forms are useful for forms that need to be archived, since these records are easier to read than the exportable submission data for other forms. Like all forms, record forms submissions can still be exported as .json or .csv files.