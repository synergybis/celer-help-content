### Save and Resume
Save and resume style forms will automatically save any data entered, allowing you to exit the form and return to it without losing data. These can be created by selecting “Save and Resume” form style   when creating a form, or by changing to a save and resume style under form settings in Celerform Administration. For more information on Save and Resume forms, visit [Form Types](/form_types).

### Collaboration Forms
Collaboration style forms will automatically save any data entered by the submitter in the same way that save and resume forms do. Submitters can exit and return to their form later when needed without losing information already entered.

### Exporting Forms
The settings   menu in the top left of Celerform Designer allows you to export your form as a .json file, which can then be re-imported into Celerform later or into any other program that accepts .json formats.

### Records Forms
Records forms are designed to save multiple records to local storage, or to another application via API. Record forms are created by selecting “Records Style”   when creating a form, or by changing to a records style under form settings in Celerform Administration. For more information on records forms, visit [Form Types] or [APIs].