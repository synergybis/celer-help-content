## Form Types
Celerform has three different form types which can each use 4 different form styles, making for a total of 12 different form combinations. The three form types are basic form, grid form, and multi-page form. The four different form styles are survey style, save and resume style, collaboration workflow, and records style.

<details><summary>Basic Form</summary>
The basic form is the simplest type of form and lets you create forms one component at a time. These forms are constructed one component at a time, and don’t have any special features, like a grid, or a navigation feature. These are best suited for basic surveys.</details>

<details><summary>Grid Form</summary>
Grid forms are used to create forms with rows and columns. They are created using [Grid Row] container components. Each grid row container functions as a row for the grid. Placing a component in the grid row will place the component in its own column. Since each grid row is its own component, each row can have a unique number of columns. Grid forms are best suited for pdf-style forms.</details>

<details><summary>Multi-Page Form</summary>
Multi-Page Forms are like basic forms, but they have a navigation bar at the top of the form. Quick links can be added to the navigation bar by checking the add to navigation header checkbox.  Clicking this quick link will scroll the form down to the container linked. Multi-Page forms are best suited for extremely long forms that need a table of contents.</details>

<details><summary>Survey Style</summary>
Survey Style forms are the simplest form style. These forms will accept submissions when the submission button is clicked. They do not require a Celerform login by default, and can accept multiple submissions from the same submitter. These are best used for short surveys or basic forms.</details>

<details><summary>Save and Resume Style</summary>
Save and Resume forms are like Survey Style forms, except that they will retain information placed into them prior to submission. If needed, users can refresh the form’s page and their information will remain in the form. These are best used for long forms that require a lot of information.</details>

<details><summary>Collaboration Workflow</summary>
Collaboration Workflow forms are forms that require validation by a reviewer. Users must be assigned as either submitters or reviewers to this type of form. Once assigned, submitters can fill out and submit forms for review in Celerform Runtime, and their responses will be sent to reviewers. Reviewers can then go to Celerform Runtime and review responses. Reviewers can either accept or reject responses, and can mark individual components as accepted or rejected. If a submission is rejected, it is returned to the submitter to edit. If a submission is accepted, the submission will go to the submission section of the form’s administration page. These are best used for document information submission, or other information that demands high accuracy.</details>

<details><summary>Records Style</summary>
Records style forms are forms that keep copies of all entered submissions in a records folder. Unlike all other form types, following the link for a record form brings up the archive of records, or submissions, instead of the form itself. Records can then be viewed, or they can be added by clicking the   add button. Record forms will then contain a toolbar at the top of the form, which allow the user to view the previous or next record  , save the record  , add a new record  , delete the record  , reset the record  , or return to the form’s home archive  . Records forms are useful for forms that need to be archived, since these records are easier to read than the exportable submission data for other forms. Like all forms, record forms submissions can still be exported as .json or .csv files.</details>