### Creating a rule
Logic rules can be created in Celerform using the rules editor. Rules have a wide variety of applications, most of which can be found under Symantec Rules in the Help section of Designer. Here we will go over the basic process of creating a rule.
1)	Select the Rules Editor from the dropdown menu in the top left-hand corner of the Form Editor. This will bring up the rules editor.
2)	Click the primary widget that will be activating the rule. This can be any type of widget, but we recommend that this is the main or one of the main widgets that will interact with the rule.
3)	Click the create button, this will bring up the rule creation menu.
4)	You’ve now created a blank rule. The effects of each rule are created from this menu. For instructions on how to create specific types of rules, see the examples below.

### Creating a duplication rule
Rules in Celerform can be used to duplicate data into another textbox. We’ll start with this type of rule since it’s the simplest one to make.
1)	Create a rule as shown in the previous section.
2)	On the rule creation menu, scroll down to the Then category.
3)	Click the generic-update rule component.
4)	In the update component, select the widget in which you want to duplicate information.
5)	Select form component data from the select option dropdown menu on the right. This indicates to Celerform that we will be updating the widget with data fed into a component.
6)	Finally, we’ll select our initial widget as our form data source.
This creates a rule that takes any data put into the initial widget and updates a second widget with it. Rules are immediately saved and can be tested in the form editor once created.

### Creating a rendering rule
Rules can toggle if certain widgets in a form are hidden or visible.
1)	Create a rule as shown in the previous section.
2)	On the rule creation menu, scroll down to the Then category.
3)	Click on the rendering-toggle rule component.
4)	Select the widget you want to show or hide.
This rule is now complete. This is most easily done with a toggle component since they will toggle a rule on or off depending on if they are checked or unchecked. Widgets that are hidden when the toggle is off will be unhidden when it is turned on, and unhidden widgets will be hidden when the toggle is activated.

### Creating a disabling rule
Rules can toggle if widgets are enabled or disabled. These rules are nearly identical to rendering rules.
1)	Create a rule as shown in the previous section.
2)	On the rule creation menu, scroll down to the Then category.
3)	Click on the disabling-toggle rule component.
4)	Select the widget that you want to enable or disable.
This rule is now complete. This rule functions nearly identically to rendering toggle rules, but will toggle whether a component is disabled or not.

### Creating a clearing rule
Clearing rules are rules that let you clear components of any filled in data. This can be useful for things such as buttons that will clear an entire form in case submitters want to change all of their data.
1)	Create a rule as shown in the previous section.
2)	On the rule creation menu, scroll down to the Then category.
3)	Click on the generic-clear rule component.
4)	Choose the widget that you want to clear of data.
This rule is complete. Whenever this rule is activated, the components that are set to clear will clear any information within them. Clear rules will override default values of widgets.

### Creating a sentence builder
Celerform can add strings of letters or words together to create larger sentences.
1)	Create a rule as shown previously.
2)	Click the Additional Sources button. Additional sources allow the rule to be activated by more than one widget. This is useful if you are adding the contents of multiple widgets together, both widgets should be able to activate the rule.
3)	Click the add icon under the additional sources options and add the other widget that should activate the rule.
4)	Scroll down to the Then category.
5)	Click on the string-add rule component.
6)	You can now select what kind of strings you want to add together. The three main options are result from previous rule, form component data, and static text. Form component data will take the text typed into a widget and add it to the rule. Static text will take text typed directly into the rule by the rule creator and add that to the rule. Result from previous rule will take the final text created by a prior string addition rule and add it to the rule. For example purposes here, we’ll be adding two instances of form component data and a static text, but note that you can add these together in any combination you’d like.
7)	For our example, we’ll first select form component data from the first drop-down in the string-add component. Select the widget from which you want your text data to come from.
8)	Select static text from the second string-add dropdown and type in the text you want added. In this example, we’ll be adding a space. Note that if two form component data components are directly added, Celerform will not automatically put a space between them (e.g., adding a first and last name together will produce firstnamelastname, rather than firstname lastname).
9)	To add more string components, click the plus button in the string-add component. Added components will default to form component data, but this can be changed by clicking the wrench icon. In our example, we’ll simply select our second widget to pull data from.
10)	Once all text components are added to the string addition rule, an update component is needed to print the rule’s result out. Without an update component, the rule will add the strings together, but won’t actually show them anywhere in the form.
11)	Select the add button at the bottom of the Then section. This will bring up the list of then components that you can add to the rule.
12)	Select generic-update to create an update component.
13)	Select the widget you want to have display the result of the string addition rule in the first box in the update component.
14)	In the second box, select result from previous rule from the drop-down menu. This will pull up another drop-down menu inside the result from previous rule section. Select add from this drop-down.
The rule will now add our two input widgets together, put a space between them, and display the result in our output widget. The rule is complete and can be seen in action in the form editor.

### Mathematical rules – addition
Celerform is capable of addition, subtraction, multiplication, and division. We’ll go over how to create each of these rules, starting with addition. Similar to string rules, mathematical rules can use both data from the form, or pre-set data created by the rule creator.
1)	Create a rule as shown previously.
2)	Click the additional sources button, followed by the add icon to add any other inputs to the math rule. If you’re using more than one input in the form for the math rule, the other inputs should be added to the rule so that the rule will activate when they are typed in.
3)	Scroll down to the Then section. Select the number-add rule component.
4)	Click the first drop-down menu to select the first number type you want to add. There are multiple available types, but the most common are form component data, results from previous rules, and static numbers. Form component data is the information typed into a widget, results from previous rules are the number outputs of a previously made rule, and static numbers are numbers entered by the rule creator that don’t change.
5)	If you selected form component data, choose the first widget that you want to have input a number to add. If you selected static number, simply type in the number you want the rule to add. We’ll get to result from previous rule in a couple more steps.
6)	Select the second type of number you want to add from the second drop-down and type in the widget or number you want to add.
7)	To add more than two numbers together, click the plus icon inside of the add component. This will add another number field to add to the rule. By default, new number fields contain form component data, but this can be changed by clicking the wrench icon.
8)	Once you’ve added all the components to add, an update component is needed to display the result of the addition rule. Select the add button at the bottom of the Then section. This will pull up the list of possible then components.
9)	Select the generic-update component.
10)	The first update drop-down will be labelled form component data, select the widget you want to have display the results of the rule.
11)	Select the second drop-down in the update component and select result from previous rule.
12)	Choose the add option from the result from previous rule dropdown.
This rule is now complete. It will add any numerical values in your widgets with any static numbers you’ve chosen and display the result in the updated widget.

### Mathematical rules – subtraction
Subtraction rules are mathematical functions that take an initial number and subtract any succeeding number from it. Subtraction rules can use both data from the form, or pre-set data created by the rule creator.
1)	Create a rule as shown previously.
2)	Click the additional sources button, followed by the add icon to add any other inputs to the math rule. If you’re using more than one input in the form for the math rule, the other inputs should be added to the rule so that the rule will activate when they are typed in.
3)	Scroll down to the Then section. Select the number-subtract rule component.
4)	Click the first drop-down menu to select the initial number you want to subtract from. There are multiple available types, but the most common are form component data, results from previous rules, and static numbers. Form component data is the information typed into a widget, results from previous rules are the number outputs of a previously made rule, and static numbers are numbers entered by the rule creator that don’t change.
5)	If you selected form component data, choose the first widget that you want to have input the initial number. If you selected static number, simply type in the initial number for the rule to use. We’ll get to result from previous rule in a couple more steps.
6)	Select the first type of number you want to subtract from the initial number from the second drop-down, then type in the widget or static number.
7)	To subtract more than just one number, click the plus icon inside of the subtract component. This will add another number field to subtract from the initial number. By default, new number fields contain form component data, but this can be changed by clicking the wrench icon.
8)	Once you’ve added all the components to subtract, an update component is needed to display the result of the rule. Select the add button at the bottom of the Then section. This will pull up the list of possible then components.
9)	Select the generic-update component.
10)	The first update drop-down will be labelled form component data, select the widget you want to have display the results of the rule.
11)	Select the second drop-down in the update component and select result from previous rule.
12)	Choose the subtract option from the result from previous rule dropdown.
This rule is now complete. It will subtract all numbers from the first number in the rule and display the result in the updated widget.

### Mathematical rules – multiplication
Multiplication rules are mathematical functions that multiply different numbers together. Multiplication rules function almost identically to addition rules, but multiply instead of add their components. Multiplication rules can use both data from the form, or pre-set data created by the rule creator.
1)	Create a rule as shown previously.
2)	Click the additional sources button, followed by the add icon to add any other inputs to the math rule. If you’re using more than one input in the form for the math rule, the other inputs should be added to the rule so that the rule will activate when they are typed in.
3)	Scroll down to the Then section. Select the number-multiply rule component.
4)	Click the first drop-down menu to select the first number to multiply. There are multiple available types, but the most common are form component data, results from previous rules, and static numbers. Form component data is the information typed into a widget, results from previous rules are the number outputs of a previously made rule, and static numbers are numbers entered by the rule creator that don’t change.
5)	If you selected form component data, choose the first widget that you want to have input the first number. If you selected static number, simply type in the first number for the rule to multiply. We’ll get to result from previous rule in a couple more steps.
6)	Select the second type of number you want to multiply from the second drop-down, then type in the widget or static number.
7)	To multiply more than just two numbers, click the plus icon inside of the multiply component. This will add another number field to multiply. By default, new number fields contain form component data, but this can be changed by clicking the wrench icon.
8)	Once you’ve added all the components to multiply, an update component is needed to display the result of the rule. Select the add button at the bottom of the Then section. This will pull up the list of possible then components.
9)	Select the generic-update component.
10)	The first update drop-down will be labelled form component data, select the widget you want to have display the results of the rule.
11)	Select the second drop-down in the update component and select result from previous rule.
12)	Choose the multiply option from the result from previous rule dropdown.
This rule is now complete. It will multiply any numerical values in your widgets with any static numbers you’ve chosen and display the result in the updated widget.

### Mathematical rules – division
Division rules are mathematical functions that take an initial number and divide by any succeeding number. Division rules function similarly to subtraction rules but divide instead of subtract. Division rules can use both data from the form, or pre-set data created by the rule creator. A second form of division rules, referred to as modulus rules, function the same as division rules, except that they display remainders instead of decimals.
1)	Create a rule as shown previously.
2)	Click the additional sources button, followed by the add icon to add any other inputs to the math rule. If you’re using more than one input in the form for the math rule, the other inputs should be added to the rule so that the rule will activate when they are typed in.
3)	Scroll down to the Then section. Select the number-divide or number-modulus rule component.
4)	Click the first drop-down menu to select the initial number you want to divide from. There are multiple available types, but the most common are form component data, results from previous rules, and static numbers. Form component data is the information typed into a widget, results from previous rules are the number outputs of a previously made rule, and static numbers are numbers entered by the rule creator that don’t change.
5)	If you selected form component data, choose the first widget that you want to have input the initial number. If you selected static number, simply type in the initial number for the rule to use. We’ll get to result from previous rule in a couple more steps.
6)	Select the first type of number you want to divide from the initial number from the second drop-down, then type in the widget or static number.
7)	To divide by more than just one number, click the plus icon inside of the divide or modulus component. This will add another number field to divide the initial number by. By default, new number fields contain form component data, but this can be changed by clicking the wrench icon.
8)	Once you’ve added all the components to divide by, an update component is needed to display the result of the rule. Select the add button at the bottom of the Then section. This will pull up the list of possible then components.
9)	Select the generic-update component.
10)	The first update drop-down will be labelled form component data, select the widget you want to have display the results of the rule.
11)	Select the second drop-down in the update component and select result from previous rule.
12)	Choose the divide or modulus option depending on which you are using from the result from previous rule dropdown.
This rule is now complete. It will take your initial value and divide it by any subsequent numbers in the rule, then display it in the result widget.

### Rule conditions
Rules can be set to only activate under certain conditions, like if the data of two components are equal, or if a date is within a certain time period. This is where the When section of the rules engine is used. Rules that use when conditions will only activate the Then components when the When conditions are fulfilled. When conditions can also be used to activate Else conditions, which will activate when the When conditions are not fulfilled. Else rule components are identical to Then rule components. Since when based rules are conditional, there are countless rule types you can create with these. We will go over a couple of examples of how to use these in this document, but for more detailed information on all when widgets and their usage, see Symantec Rules, under help.

### Email check example
This example will show how to create a form that checks if two emails are identical. In this example, we’ll have two textboxes, and a toggle that will activate when the two emails are identical.
Insert screenshots of this form
1)	Create a rule as shown previously.
2)	Click the Additional Sources button followed by the add icon. Then add the second email widget as an additional source, this way, either widget can be used to activate the rule.
3)	Click the When Block button to access the when rule components.
4)	Click the generic-equal component.
5)	The equal component will have two sections for equal values. Select form component data for both and select the first email widget from the first form component dropdown, and the second email widget from the second dropdown.
6)	Click the add button at the bottom of the when section.
7)	Click the string-contains component. This will make sure that our components contain an @ symbol, to make sure that these are valid emails. Select form component data from the first string-contains dropdown and select one of the email validation widgets. Choose static text from the second string-contains dropdown and type in the @ symbol.
We now have our rule conditions set up, the rule checks to make sure that the two fields are equal and contain an @ symbol. The rule can be completed with then and else components, which will determine what the rule does. Suggestions for what to do when emails match include activating a toggle checkbox to confirm they are equal, or updating a textbox with text informing the user that emails match.

### Exporting a form
Celerform forms can be exported from designer in a .json file format. These files can then be re-imported into Celerform or into any other program that accepts .json files.
1)	Select the form in Celerform Designer that you want to export.
2)	Click the settings button in the top left-hand corner of the form, this will open the settings menu.
3)	Click the export form config button. This will download the form’s configuration data as a .json file.