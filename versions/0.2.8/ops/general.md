Note: this is the early draft of the step-by-step how to file for everything in Celerform. We may want to break this up or even set it to be in another file type.

This document contains step-by-step instructions on how to do everything in Celerform.
## Creating forms in Celerform
### Creating a default-style survey form
**Step 1 - Creating the form**

Click the default style button on the Celerform Designer homepage. Type a name for the form into the Form Namespace textbox and click the Start Building button.

**Step 2 - Adding widgets**

Click the Add icon under the Data Components section of the form designer and select the type of widget you want to add. The widget will appear as a Data Component. Data Components can be dragged into the form outline as often as you'd like to create multiple copies of the same widget. Additional types of widgets can be added by adding different Data Components by clicking the Add icon again.

**Step 3 - Publishing the survey**

When you are finished editing your form, click the Share icon in the upper left corner of the form editor. This will bring up the sharing menu. In this menu, click the Publish button to publish the form. The form will immediately be accessible using the link displayed in the Publish menu.
### Creating a grid-style form
Click the grid style button on the Celerform Designer homepage. Type a name for the form into the Form Namespace textbox and select the form type desired, survey style, save and resume, record, or collaboration form. More information is available on these types later in this guide. Click the Start Building button to create the form.
### Creating a government-style form
Click the government style button on the Celerform Designer homepage. Type a name for the form into the Form Namespace textbox, and select the desired form type: survey, save and resume, record, or collaboration form. More information is available on these types later in this guide. Click the Start Building button to create the form.
### Creating a wizard-style form
**Step 1 - Creating the form**

Click either the default or government style button on the Celerform Designer homepage. Type a name for the form into the Form Namespace textbox and select the type of form, survey, save and resume, record, or collaboration. Select the wizard form option from the form widget dropdown and click the Start Building option.

**Step 2 - Creating pages**

Click the add icon under the Data Components section and click container. This will create a reusable container data component, drag and drop it into the Form Outline. This container will function as the first page of the form. Create the data components you want to add to the first page and drag and drop them into the container. When you want to add a second page, add another container from Data Components into the Form Outline. Each added container will function as another page in the order that they appear in the form outline.

**Step 3 - Publishing the form**

Like any other form, when you are finished editing your form, click the Share icon in the upper left corner of the form editor. This will bring up the sharing menu. In this menu, click the Publish button to publish the form. The form will immediately be accessible using the link displayed in the Publish menu. Wizard style forms will immediately display themselves as multiple page forms
### Creating a save and resume form
Select the desired style of form, default, grid, or government you want to create. Enter a name into the Form Namespace textbox and select the Save and Resume button to create a save and resume type form. Click the Start Building button to create the form. When save and resume forms are published, they will save information typed into them prior to submission by the submitter.
### Creating a record form
Select the desired style of form, default, grid, or government and enter a name into the Form Namespace textbox. Select the Record button and click the Start Building button. Once published, record forms start in the records menu, which allows users to manage copies of the record form. Record forms can be added, deleted or edited from this menu. For more information on how to use record forms, see the [Records Forms] section.
### Creating a collaboration form
Select the desired style of form: default, grid, or government and enter a name into the Form Namespace textbox. Select the Collaborate button and click the Start Building button. Once published, Collaborate forms only allow assigned users to access the form through the link or through Celerform Runtime. For information on how to use collaboration forms, see [Collaboration].
## Editing forms
### Creating form components
Form components can be added from the add button in the form outline, or the add button in data components. Container, progress bar, label and navigation components can be created from the add icon under form outline. Click the add button and select which type of component you want to add, and it will appear as the next component in the form preview. Clicking the add button under data components allows for adding attachment, choice, date, toggle, number, text, signature, container, list and other component types. Clicking one of these will add that type in as a data component. Data components can then be dragged and dropped into the form outline to add them to the form.
### Changing the order of components
Components can be dragged and dropped in the form outline to rearrange the order that they appear in on the form.
### Changing component types
Existing components' types can be changed into any other component type. For example, a text box can be changed into a number box. Select the component and clicking the edit icon under type in the menu on the right side.
### Changing component widgets
Either existing form components or data components can have their widgets changed. Select the component you want to change and click the widget dropdown in the menu on the right. The widget dropdown displays every available widget for that component type, select the desired widget and the component will automatically update.
### Adding components to a container
Add a container component to the form outline by clicking the add button and selecting container from the dropdown menu. Components can now be dragged and dropped from either the form outline or data components into an existing container. Containers can have as many components placed into them as desired, and can be edited in the same ways any other component can be.
### Creating grid rows
Grid rows can be created by adding a container component to the form outline in grid style forms. Containers added to grid style forms default to being grid rows. Components added to grid rows will appear in the row each as it's own column. Grid rows can have as many columns as the designer wants, however, we recommend not putting more than 8 components into a grid row.
### Changing between survey and widget form types
Forms can be switched between survey and widget types by clicking the cog icon in the upper left corner to open the settings menu in the Form Editor. Click the Form Widget dropdown and select either survey or wizard. Note that when switching, containers will switch behavior to either regular containers in the case of a survey, or pages in the case of a wizard form.
### Adding first and last buttons to a wizard form
Click the cog icon in the upper left corner of the Form Editor to open the settings menu. Click the First and Last Buttons active checkbox to enable first and last buttons in the published form.
## Rules
### Creating a rule
From the form editor, click the dropdown menu in the upper left corner of the screen and select the rules editor. Select the first component needed for the rule and click the create button. This will create the rule and bring up its rule editing page.
### Renaming a rule
In the rule's editing page, clear the rule title textbox and type in the name of the rule. Rules are automatically just named Rule, and naming them will help organize rules.
### Triggering rules
Rules can be triggered in two ways, either upon loading the form, or by interacting with a component. By default, rules are activated when a component is interacted with. The component used to create the rule is automatically considered a trigger for the rule, but additional triggers can be added under additional sources.
## Publishing forms
### Publishing a form for the first time
Publishing forms is done through the share menu in the upper left corner of the form designer. Click the share icon in the upper left corner to bring up the share menu and click the Publish button. The publish menu will display a link and a copy icon. Clicking the copy icon will copy the link, which will take you directly to the published form.
### Updating a published form
Updating a published form is done by editing the form in designer and accessing the share menu by clicking the share icon. Check the activate form version on publish checkbox and click the publish button. The form will automatically update to the new version of the form.
### Publishing an old version of a form
Go to Celerform Administration and click on the View Forms link in the sidebar on the left. The forms menu will bring up a list of all published forms in alphabetical order. By default the list only shows the first 25 entries, but can show up to 100. There is also a search function that works regardless of how many forms there are published. Click the form to navigate to the form's administration menu. Select the versions tab to bring up all prior versions of the form. Form versions are shown in order of publish date. The currently active version of the form will display as active, all others will display as inactive. Select the icon on the right side of the screen that corresponds to the version you want to activate. Then select the activate version button from the dropdown menu. The form will immediately update to the old version. Celerform retains all versions of a form created, and versions can be deactivated or reactivated at any time.
###