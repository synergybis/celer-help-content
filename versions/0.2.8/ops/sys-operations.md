**Submissions**

Submissions can be viewed or removed through the submissions tab. Individual submissions can be viewed either as they appear in the form, or in raw data form, under the data subtab. The raw data can be copied and pasted to other locations without exporting all submissions from the form. Signatures and uploaded files can be viewed under submissions, and files can be downloaded directly from their submission.

**Analytics**

Analytics aggregates all responses to a version of a form. These responses can be viewed individually, or in the form of graphs. Responses can also be exported in .json or .csv formats via the export tab, or directly printed from the print option. Responses to questions can be organized using graphs, including bar, line, and pie graphs, or as a likert field. Analytics analyze responses to one version of each form, and can be switched between versions to view past responses.

**Sorting Data**

Submission data can be sorted by a variety of categories.

**Export**

Submission data can be exported with question data in .json format, or without the question data in .json or .csv format. Forms themselves can be exported from Celerform Designer as .json files.

**Versions**

Each publish of a form creates a new version of the form. Previous versions can be activated from the versions tab. Previous version submissions can be viewed from the analytics tab.

**Form Types**

Celerform contains 4 different form types: survey forms, save and resume forms, collaboration forms, and records. The form type is chosen when first creating a form in Celerform Designer. The type can be changed after publishing under settings in Celerform Administration.

**Form Access**

Forms can be accessed by users by default, but form access can be deactivated from settings in Celerform Administration. Dates that forms will be accessible can also be changed from settings, which will activate the form on the starting date, and deactivate the form on the ending date.

**User Authentication**

User authentication determines whether a user needs to be logged into a Celerform account or not to fill out a form. Records and collaboration forms require user authentication by default. User authentication can be activated or deactivated from settings.

**Assigning Users**

Collaboration style forms require users to be assigned to the form to interact with it. Users can be assigned to forms under the form's users sub-tab. Users can be assigned as read-only, submitter, reviewer, or admin. Read-only users can't submit anything in the form. Submitters can submit one submission through Celerform Runtime. Reviewers can review and either approve or reject submissions. Admins can either submit or review submissions to the form. Collaboration forms require at least one submitter and one reviewer to function correctly.

**Logs**

Celerform Administration automatically logs file uploads, user creation and user deletion.

**Users**

Users can be added, deleted, and modified from Celerform Administration. Users can be assigned groups or roles to differentiate user permissions. If needed, administrators can reset users’ passwords.

**Roles**

Roles are permissions that can be assigned to groups or users. A number of system default roles exist, including read, edit and delete roles for Celerform designer, admin and runtime. New custom roles can be created and edited by users with the role editing role

**Groups**

Groups can be created, edited, and removed under Celerform Administration. Groups are used to group users together and assign them all identical roles.

**Theming**

Celerform’s theme can be changed from Celerform Administration. Themes included light mode, dark mode, dark blue, and USCG.