**API**

Celerform can connect to third party API’s utilizing the OpenAPI Specification, a standardization of REST API calls. These API’s can then be configured to be used within the form for a myriad of reasons. One such example is third party database integration; another would be validation or data-lookup rules. 

Within the designer, an OpenAPI Schema can be imported to kick-start a form that relies on this third-party integration. 

Within admin, third party database integration can be configured easily for all form types, allowing no data to be stored within the Celerform platform. 

Of note, all API calls are called securely within Celerform’s micro-services, API keys, among other sensitive data is not exposed to the user’s client, and all data transferred from the user’s client and the services is done via https.

**Low Code Usability**

Celerform can be operated with no knowledge of coding, including form editing. Published forms can be edited in Celerform Designer and re-published. Editing and publishing a form will create a new version of the form that can be activated or deactivated from Celerform Administration.

**Updates**

Celerform receives regular security updates. Available updates and descriptions of updates are available on Celerform Administration's homepage.

**Support**

Add support to Celerform, including bug reporting.

**User Authorization**

Celerform Administration can assign users to forms. Users who are not assigned to forms requiring user authentication cannot view these forms.

**Browser Requirements**

Celerform requires a JavaScript-enabled browser and supports the current and prior major releases of Google Chrome, Mozilla Firefox, Apple Safari, Microsoft Edge, and most mobile browsers.

*Notes :: Google Chrome version 1.1 + (random version)*

**Modernization Efforts**

*Add this once it's implemented (is this going into general documentation?)*

**CAC Security**

*Add once this is implemented (again, will this part go out to any customer, or just USCG?)*