![](../../../images/Then-Generic-Clear.png)

Clears data from a widget when the rule is activated.
