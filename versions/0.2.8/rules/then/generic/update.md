![](../../../images/Then-Generic-Update.png)

Enables a disabled widget when activated.
