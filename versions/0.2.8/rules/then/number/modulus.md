![](../../../images/Then-Number-Modulus.png)

Divides numbers and shows remainder instead of a decimal. Can use numbers from widgets, set numbers, and numbers from previous rules.
