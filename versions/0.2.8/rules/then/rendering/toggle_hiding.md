![](../../../images/Then-Rendering-Toggle.png)

Switches a widget between being visible and hidden, depending on if the rule is active or not. The rule starts with the widget’s default state in the form (eg. a hidden widget will become visible when the rule is active, and a visible widget will be hidden when active).
