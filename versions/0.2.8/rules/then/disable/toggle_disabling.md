![](../../../images/Then-Disable-Toggle.png)

Enables or disables a widget when the rule is activated or deactivated.