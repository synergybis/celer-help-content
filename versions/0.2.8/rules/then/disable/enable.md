![](../../../images/Then-Disable-Enable.png)

Enables a disabled widget when activated.
