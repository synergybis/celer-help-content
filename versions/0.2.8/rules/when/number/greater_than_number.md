![](../../../images/When-Number-Greater-Than.png)

Activates the rule when the widget contains a number with a higher value than a set number, or a number in another widget.
