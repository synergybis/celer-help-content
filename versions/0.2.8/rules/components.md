Rules are controlled by triggers, when, then, and else events. Triggers are the components that cause the rule to activate, then components dictate what the rule will do when activated, and the else events dictate what the rule will do when inactive.

## Triggers

![](../images/Rule%20Trigger.png)
There are two types of rule triggers in Celerform, on change, and on init. On change triggers will trigger the rule when the trigger component is changed and on init triggers will activate when the form is loaded. If you want multiple components to trigger the rule, select the “Additional Sources” button, and select the other component(s) you want as triggers. Rules will be updated any time any one of the triggers selected is interacted with.

## When

![](../images/When%20Block.png)
When rule components determine the conditions that activate a rule. These include whether a widget is empty, if the value of the widget being equal to another, conditional logic, such as the value being greater than or less than a number, or comparing dates. The different when component types are Generic, Date, List, Number, and String. Details on each component can be found under [Rule Components]().

## Then

![](../images/Then%20Block.png)
Then rule components determine what the rule will accomplish. A full list of these can be seen under [Rule Components](). The different then component types are Generic, Number, String, Disable, Rendering, and Endpoint.

## Else

![](../images/Else%20Block.png)
Else rule components are identical to then rule components, but are triggered when the rule is considered “turned off,” or “false.” You can consider these components equivalent to a “not” component. Else components will trigger whenever the rule’s conditions are unfulfilled, and are best used in combination with not components. There are examples of using else rules under [Example Rules](examples.md).
