The celer rules engine allows business users to write rules on dynamic
form components. These rules define actions that trigger based on user
interaction with the form. These interactions include both user input
and user actions. These rules can help streamline the form filling
experience, focusing on providing simplicity and accuracy to both the
form filler, and the submitted data.

In order for business users to write rules, the rules editor is an
simplified user interface to write and configure rules. The visual
editor allows these users to write highly configurable and diverse
rules, such as:

- Conditionally render a form component
- Enable or disable a form component
- Set a value for a form component
- Display error messages on an form component
- Modify a property of a form component
- Perform operations on form data
- Validate the user's inputted data
  Invoke API's' to preform validation
- Invoke API's to provide autofill data
