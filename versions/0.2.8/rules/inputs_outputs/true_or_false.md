![](../../images/True-False.png)
True or false inputs check if a widget or condition is true or false. This can be used to check if a toggle is checked or not, or can be used to check if a rule is fulfilled.
