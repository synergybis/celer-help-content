![Math]()
Math rules allow you to construct forms that allow for multiple different types of mathematical calculations. Calculate rule components allow users to enter algebraic equations using variables. Operations that can be calculated include addition, subtraction, multiplication, division, trigonometric functions, exponential functions, natural logarithm, e^x and rounding.

[String Addition]

![String Addition](../images/Example-String-Addition.png)
String addition lets you add together strings of text. Like other rules that produce results, an update component should output the “Result From Previous Rule.” Remember to add a static empty space if you want the result not to be fused together (eg: getting “firstname” vs “first name” as your result).

[Duplication]

![Duplication Example](../images/Example-Duplication.png)
Duplication can be done with a simple update field. Update your duplicated text field from your original text.

[Hiding and Disabling]

![Hiding](../images/Example-Hide.png)
Hiding and disabling rules is done most easily with toggle rule components. The toggle rule component will update the target widget, no update rule component is required.